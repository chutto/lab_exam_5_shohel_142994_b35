<?php
class MyCalculator {
    private $val, $val1;

    public function __construct( $val, $val1 ) {
        $this->val = $val;
        $this->val1 = $val1;
    }
    public function add() {
        return $this->val + $this->val1;
    }
    public function subtract() {
        return $this->val - $this->val1;
    }
    public function multiply() {
        return $this->val * $this->val1;

    }
    public function divide() {
        return $this->val / $this->val1;
    }
}
$mycalc = new MyCalculator(16, 2);
echo "the Addition result is ..........:-"."\t". $mycalc-> add()."<br>";
echo "the  multiplication result is.........:-"."\t".$mycalc-> multiply()."<br>";
echo "the Substraction result is..........:-"."\t".$mycalc-> subtract()."<br>";
echo "the division result is...........:-"."\t".$mycalc-> divide()."\n";
?>